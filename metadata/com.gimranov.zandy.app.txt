Categories:Office
License:AGPLv3
Web Site:http://www.gimranov.com/avram/w/zandy-user-guide
Source Code:https://github.com/ajlyon/zandy
Issue Tracker:https://github.com/ajlyon/zandy/issues

Auto Name:Zandy
Summary:Library assistance
Description:
Access your Zotero library from your mobile device. Edit and view your library, sync, and work offline.
Zandy provides a simple interface to all your research. Browse and modify the items in your library,
add new items, view attachments, take and edit item notes, search your library, add webpages from
the Android browser and more.

The barcode functionality requires [[com.google.zxing.client.android]] to be installed.
.

Repo Type:git
Repo:https://github.com/ajlyon/zandy.git

#The author must have forgot to bump manifest for the tag
Build:1.3.6,20
    commit=1.3.6
    subdir=app
    forceversion=yes
    forcevercode=yes
    extlibs=android/android-support-v4.jar

Build:1.3.7,1370
    commit=1.3.7
    subdir=app
    extlibs=android/android-support-v4.jar

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.3.7
Current Version Code:1370

