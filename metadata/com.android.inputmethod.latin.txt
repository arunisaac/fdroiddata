Categories:System
License:Apache2
Web Site:
Source Code:https://android.googlesource.com/platform/packages/inputmethods/LatinIME
Issue Tracker:https://code.google.com/p/android/issues/list?can=2&q=keyboard&colspec=ID+Type+Status+Owner+Summary+Stars&cells=tiles

Summary:Stock keyboard
Description:
Despite the splash screen, there is no gesture typing in 4.4.2.
There is only limited amount of monochrome Emoji characters,
though they may be supplemented if you have another keyboard installed.
Other recent differences include: white hinting, quicker entry of user-defined
words, better suggestions in landscape and layout changes.

The current version comes with English, Spanish, Russian, PT-Brazilian, Italian,
German, French, dictionaries; there are more language word lists
in the repository but they'd need to be compiled.

Of course, you can't install this if you have a ROM that already has
the com.android.inputmethod.latin package installed.
You could build it yourself with a different package name, but you might need
to change the authority string for dictionary packs too; this may be fixed in the next builds.
Otherwise, you'll have to remove the original from /system/app (and dalvik-cache)
via root, before installing this.
.

Repo Type:git
Repo:https://android.googlesource.com/platform/packages/inputmethods/LatinIME

Build:4.4.2-MIPS,4421
    commit=android-sdk-4.4.2_r1
    subdir=java
    target=android-19
    extlibs=android/android-support-v4.jar
    srclibs=Inputmethodcommon@android-4.4.2_r1
    patch=custom_rules.xml_1.patch
    prebuild=echo -e 'package.name=com.android.inputmethod.latin\nversion.name=4.4.2-MIPS\nversion.code=4421' >> ant.properties && \
        cp -r $$Inputmethodcommon$$/java/com/android/inputmethodcommon src/com/android/
    build=$$NDK$$/ndk-build -e "APP_ABI=mips" -e "APP_CFLAGS += -w" -C ../native && \
        mv ../native/libs/* libs/

Build:4.4.2-x86,4422
    commit=android-sdk-4.4.2_r1
    subdir=java
    target=android-19
    extlibs=android/android-support-v4.jar
    srclibs=Inputmethodcommon@android-4.4.2_r1
    patch=custom_rules.xml_1.patch
    prebuild=echo -e 'package.name=com.android.inputmethod.latin\nversion.name=4.4.2-x86\nversion.code=4422' >> ant.properties && \
        cp -r $$Inputmethodcommon$$/java/com/android/inputmethodcommon src/com/android/
    build=$$NDK$$/ndk-build -e "APP_ABI=x86" -e "APP_CFLAGS += -w" -C ../native && \
        mv ../native/libs/* libs/

Build:4.4.2-ARM,4423
    commit=android-sdk-4.4.2_r1
    subdir=java
    target=android-19
    extlibs=android/android-support-v4.jar
    srclibs=Inputmethodcommon@android-4.4.2_r1
    patch=custom_rules.xml_1.patch
    prebuild=echo -e 'package.name=com.android.inputmethod.latin\nversion.name=4.4.2-ARM\nversion.code=4423' >> ant.properties && \
        cp -r $$Inputmethodcommon$$/java/com/android/inputmethodcommon src/com/android/
    build=$$NDK$$/ndk-build -e "APP_ABI=armeabi" -e "APP_CFLAGS += -w" -C ../native && \
        mv ../native/libs/* libs/

Build:4.4.2-ARMv7,4424
    commit=android-sdk-4.4.2_r1
    subdir=java
    target=android-19
    extlibs=android/android-support-v4.jar
    srclibs=Inputmethodcommon@android-4.4.2_r1
    patch=custom_rules.xml_1.patch
    prebuild=echo -e 'package.name=com.android.inputmethod.latin\nversion.name=4.4.2-ARMv7\nversion.code=4424' >> ant.properties && \
        cp -r $$Inputmethodcommon$$/java/com/android/inputmethodcommon src/com/android/
    build=$$NDK$$/ndk-build -e "APP_ABI=armeabi-v7a" -e "APP_CFLAGS += -w" -C ../native && \
        mv ../native/libs/* libs/

Maintainer Notes:
Note that, like other AOSP apps, there are now special tags with 'sdk' in them;
not sure what the difference is.

Todo for next versions:
  Change authority string in res/values/dictionary-pack.xml,  à la
    android:authorities="@string/authority" to allow for installation on top of other apps.
  Split up packages based on user dictionaries.
.

Archive Policy:8 versions
Auto Update Mode:None
Update Check Mode:None
Current Version:4.4.2
Current Version Code:4424

