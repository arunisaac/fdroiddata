Categories:Multimedia
License:GPLv3+
Web Site:http://homepage.ntlworld.com/mark.harman/comp_wifiremoteplay.html
Source Code:http://sourceforge.net/p/wifiremoteplay/android_native
Issue Tracker:http://sourceforge.net/p/wifiremoteplay/tickets
Donate:http://sourceforge.net/donate/?user_id=439017
Bitcoin:1LKCFto9SQGqtcvqZxHkqDPqNjSnfMmsow

Auto Name:Wifi Remote Play
Summary:Media remote control
Description:
Remote Control for MPC (Music Player Daemon) and VLC.
.

Repo Type:git
Repo:git://git.code.sf.net/p/wifiremoteplay/android_native

Build:1.11,7
    commit=v1.11

Build:1.12,8
    commit=v1.12

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.12
Current Version Code:8

