Categories:Internet
License:GPLv3
Web Site:http://www.kontalk.org
Source Code:https://github.com/kontalk/androidclient
Issue Tracker:https://github.com/kontalk/androidclient/issues
Donate:https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=VU57LMG64GATN
Bitcoin:14vipppSvCG7VdvoYmbhKZ8DbTfv9U1QfS

Auto Name:Kontalk
Summary:Community-driven messaging
Description:
Kontalk is an instant messaging system - primarly designed for embedded
devices, especially smartphones. It is designed to be distributed, based on
community-driven infrastructure.
Send and receive messages for free with other Kontalk users.
Kontalk uses your phone number to identify yourself and automatically adds
other Kontalk users you can talk with by looking in your contact list.
You can send any text messages and image (other media types coming soon).
Even in server-to-client communication, your phone number is irreversibly
encrypted, so even the server can't know your phone number: it is used only
for sending you the verification code, then it will be discarded.
.

Repo Type:git
Repo:https://github.com/kontalk/androidclient.git

Build:2.2.5,21
    commit=2.2.5
    srclibs=ActionBarSherlock@4.2.0
    prebuild=sed -i 's@\(android.library.reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/ && \
        wget "http://protobuf.googlecode.com/files/protobuf-2.4.1.tar.gz" && \
        tar -xzf protobuf-2.4.1.tar.gz && \
        cd protobuf-2.4.1 && \
        ./configure && \
        make && \
        cd java && \
        $$MVN3$$ package -P lite && \
        cp target/protobuf-java-2.4.1-lite.jar ../../libs/ && \
        cd ../../ && \
        rm -rf protobuf-2.4.1*

Build:2.2.6,23
    commit=2.2.6
    srclibs=ActionBarSherlock@4.2.0
    prebuild=sed -i 's@\(android.library.reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/ && \
        wget "http://protobuf.googlecode.com/files/protobuf-2.4.1.tar.gz"
    build=tar -xzf protobuf-2.4.1.tar.gz && \
        cd protobuf-2.4.1 && \
        ./configure && \
        make && \
        cd java && \
        $$MVN3$$ package -P lite && \
        cp target/protobuf-java-2.4.1-lite.jar ../../libs/ && \
        cd ../../ && \
        rm -rf protobuf-2.4.1*

Build:2.2.7,24
    commit=2.2.7
    srclibs=ActionBarSherlock@4.2.0
    prebuild=sed -i 's@\(reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/ && \
        wget "http://protobuf.googlecode.com/files/protobuf-2.4.1.tar.gz"
    build=tar -xzf protobuf-2.4.1.tar.gz && \
        cd protobuf-2.4.1 && \
        ./configure && \
        make && \
        cd java && \
        $$MVN3$$ package -P lite && \
        cp target/protobuf-java-2.4.1-lite.jar ../../libs/ && \
        cd ../../ && \
        rm -rf protobuf-2.4.1*

Build:2.2.8,25
    commit=2.2.8
    srclibs=ActionBarSherlock@4.4.0
    prebuild=sed -i 's@\(reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/ && \
        wget "http://protobuf.googlecode.com/files/protobuf-2.4.1.tar.gz"
    build=tar -xzf protobuf-2.4.1.tar.gz && \
        cd protobuf-2.4.1 && \
        ./configure && \
        make && \
        cd java && \
        $$MVN3$$ package -P lite && \
        cp target/protobuf-java-2.4.1-lite.jar ../../libs/ && \
        cd ../../ && \
        rm -rf protobuf-2.4.1*

Build:2.2.9,26
    commit=2.2.9
    srclibs=ActionBarSherlock@4.4.0
    prebuild=sed -i 's@\(reference.1=\).*@\1$$ActionBarSherlock$$@' project.properties && \
        cp libs/android-support-v4.jar $$ActionBarSherlock$$/libs/ && \
        wget "http://protobuf.googlecode.com/files/protobuf-2.4.1.tar.gz"
    build=tar -xzf protobuf-2.4.1.tar.gz && \
        cd protobuf-2.4.1 && \
        ./configure && \
        make && \
        cd java && \
        $$MVN3$$ package -P lite && \
        cp target/protobuf-java-2.4.1-lite.jar ../../libs/ && \
        cd ../../ && \
        rm -rf protobuf-2.4.1*

Build:3.0a5,31
    disable=Skip alpha

Auto Update Mode:None
Update Check Mode:Tags
Current Version:3.0a5
Current Version Code:31

