Categories:Internet
License:GPLv2+
Web Site:http://forum.xda-developers.com/showthread.php?t=2616121
Source Code:https://github.com/cernekee/ics-openconnect
Issue Tracker:https://github.com/cernekee/ics-openconnect/issues

Auto Name:OpenConnect
Summary:SSL VPN client for Cisco AnyConnect
Description:
OpenConnect for Android is an SSL VPN client used with Cisco AnyConnect or
ocserv based gateways.

Features:

* One-click connection (batch mode)
* Supports RSA SecurID and TOTP software tokens
* Keepalive feature to prevent unnecessary disconnections
* Compatible with ARMv7, x86, and MIPS devices
* No root required
* Based on the popular [http://www.infradead.org/openconnect/ OpenConnect Linux package]

Requirements:

* Android 4.0 (ICS) or higher
* An account on a suitable VPN server

This product includes software developed by the OpenSSL Project for use in the OpenSSL Toolkit (http://www.openssl.org/).

This product includes cryptographic software written by Eric Young (eay@cryptsoft.com).
.

Repo Type:git
Repo:https://github.com/cernekee/ics-openconnect

Build:0.1,1
    commit=v0.1
    submodules=yes
    prebuild=rm -rf assets/raw/{armeabi,x86,mips} libs/*.jar libs/*/*.so && \
        make -C external/openconnect/android sources
    build=make -C external NDK=$$NDK$$

Build:0.2,2
    commit=v0.2
    submodules=yes
    prebuild=rm -rf assets/raw/{armeabi,x86,mips} libs/*.jar libs/*/*.so && \
        make -C external/openconnect/android sources
    build=make -C external NDK=$$NDK$$

Build:0.5,509
    commit=v0.5
    submodules=yes
    prebuild=rm -rf assets/raw/{armeabi,x86,mips} libs/*.jar libs/*/*.so && \
        make -C external/openconnect/android sources
    build=make -C external NDK=$$NDK$$

Build:0.6,600
    commit=v0.6
    submodules=yes
    prebuild=rm -rf assets/raw/{armeabi,x86,mips} libs/*.jar libs/*/*.so && \
        make -C external/openconnect/android sources
    build=make -C external NDK=$$NDK$$

Build:0.81,819
    commit=v0.81
    submodules=yes
    prebuild=rm -rf assets/raw/{armeabi,x86,mips} libs/*.jar libs/*/*.so && \
        make -C external/openconnect/android sources
    build=make -C external NDK=$$NDK$$

Build:0.9,909
    commit=v0.9
    submodules=yes
    prebuild=rm -rf assets/raw/{armeabi,x86,mips} libs/*.jar libs/*/*.so && \
        make -C external/openconnect/android sources
    build=make -C external NDK=$$NDK$$

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.9
Current Version Code:909

