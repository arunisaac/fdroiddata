Categories:System
License:GPLv3
Web Site:http://www.androidsoft.org
Source Code:https://code.google.com/p/androidsoft/source
Issue Tracker:https://code.google.com/p/androidsoft/issues

Auto Name:Permission Friendly Apps
Summary:Rank apps by permissions
Description:
Gives a rating to each app, based on how much influence they can have.
.

Repo Type:srclib
Repo:AndroidSoft

Build:1.4.1,12
    commit=64
    subdir=permission
    target=android-15
    prebuild=rm -rf releases

Build:1.4.2,13
    commit=66
    subdir=permission
    target=android-15
    prebuild=rm -rf releases

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.4.2
Current Version Code:13

No Source Since:1.4.2.1

