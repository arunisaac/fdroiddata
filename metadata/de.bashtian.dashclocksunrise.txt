Categories:Wallpaper
License:Apache2
Web Site:https://github.com/bashtian/dashclock-sunrise
Source Code:https://github.com/bashtian/dashclock-sunrise
Issue Tracker:https://github.com/bashtian/dashclock-sunrise/issues

Name:DashClock Ext.: Sunrise
Auto Name:DashClock Sunrise Extension
Summary:
Description:
Sunrise and Sunset extension for [[net.nurik.roman.dashclock]].
.

Repo Type:git
Repo:https://github.com/bashtian/dashclock-sunrise.git

Build:1.2,3
    disable=wait until next version (at HEAD)
    commit=HEAD
    extlibs=junit/junit-4.10.jar
    srclibs=DashClock@v1.2,SunriseSunset@77c8d62dbd
    prebuild=echo 'source.dir=src;$$DashClock$$/api/src;$$SunriseSunset$$/src' >> project.properties

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.2
Current Version Code:3

